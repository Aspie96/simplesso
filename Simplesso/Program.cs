﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplesso
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] z = { 3, 2, -5, 0, 0, 0 }; // max z = 3 x1 + 2 x2 - 5 x3
            double[,] matrix = { { 4, -2, 2, 1, 0, 4 }, { 2, 1, 1, 0, 1, 1 } }; // Vincoli

            double outZ;
            double[] outVars;
            int[] outBase;
            Simplesso.Solve(out outZ, out outVars, out outBase, matrix, z, 0, 3); // Eventualmente, aggiungere altri parametri indicando una base (tanti parametri quante sono le variabili in base), ricordandosi che 0 significa x1, 1 significa x2 ecc...
            Console.WriteLine("z : " + outZ.ToString() + "\n");
            Console.WriteLine("Values:");
            for (int i = 0; i < outVars.Length; i++)
            {
                Console.WriteLine("x" + (i + 1).ToString() + " = " + outVars[i].ToString());
            }
            Console.Write("\nVariabili di base: ");
            foreach (int item in outBase)
            {
                Console.Write("x" + (item + 1).ToString() + " ");
            }
            Console.WriteLine();

            Console.Write("\nPress ESC to exit. ");
            while (Console.ReadKey(true).Key != ConsoleKey.Escape) ;
        }
    }
}
