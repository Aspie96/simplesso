﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplesso
{
    public static class Simplesso
    {
        public class SimplexException : Exception
        {
            public SimplexException() : base("Error while applying the symplex algorithm.") { }
            public SimplexException(string message) : base("Error while applying the symplex algorithm: " + message) { }
        }

        public class IncoherentProblemException : SimplexException
        {
            public IncoherentProblemException() : base("The problem is inconsistent, or doesn't have any solution.") { }
            protected IncoherentProblemException(string message) : base(message) { }
        }

        public class IncoherentBaseException : IncoherentProblemException
        {
            public IncoherentBaseException() : base("The given base is unacceptable.") { }
        }

        public class InfiniteProblemException : SimplexException
        {
            public InfiniteProblemException() : base("The problem is consistent, but it has no optimal solution.") { }
        }

        private static void Pivot(double[,] matrix, int i, int j)
        {
            if (matrix[i, j] == 0)
            {
                int row = -1;
                for (int k = matrix.GetLength(0) - 1; k > i; k--)
                {
                    if (matrix[k, j] != 0)
                    {
                        row = k;
                    }
                }
                if (row == -1)
                {
                    throw new IncoherentBaseException();
                }
                for (int k = 0; k < matrix.GetLength(1); k++)
                {
                    double aux = matrix[row, k];
                    matrix[row, k] = matrix[i, k];
                    matrix[i, k] = aux;
                }
            }
            double rat = matrix[i, j];
            for (int k = 0; k < matrix.GetLength(1); k++)
            {
                matrix[i, k] /= rat;
            }
            for (int k = 0; k < matrix.GetLength(0); k++)
            {
                if (k != i)
                {
                    double fact = matrix[k, j];
                    for (int l = 0; l < matrix.GetLength(1); l++)
                    {
                        matrix[k, l] -= matrix[i, l] * fact;
                    }
                }
            }
        }

        private static void Gauss(double[,] matrix, int[] baseVars)
        {
            for (int i = 0; i < baseVars.Length; i++)
            {
                Simplesso.Pivot(matrix, i, baseVars[i]);
            }
        }

        private static bool IsBest(double[] z)
        {
            for (int i = 0; i < z.Length - 1; i++)
            {
                if (z[i] > 0)
                {
                    return false;
                }
            }
            return true;
        }

        private static void NextBase(double[,] matrix, int[] baseVars, double[] z)
        {
            int newVar = 0;
            for (int i = 1; i < z.Length - 1; i++)
            {
                if (z[i] > z[newVar])
                {
                    newVar = i;
                }
            }
            double ratio = double.PositiveInfinity;
            int index = 0;
            for (int i = 0; i < baseVars.Length; i++)
            {
                if (matrix[i, matrix.GetLength(1) - 1] / matrix[i, newVar] > 0 && matrix[i, matrix.GetLength(1) - 1] / matrix[i, newVar] < ratio)
                {
                    index = i;
                    ratio = matrix[i, matrix.GetLength(1) - 1] / matrix[i, newVar];
                }
            }
            if (double.IsPositiveInfinity(ratio))
            {
                throw new InfiniteProblemException();
            }
            baseVars[index] = newVar;
        }

        private static bool Acceptable(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                if (matrix[i, matrix.GetLength(1) - 1] < 0)
                {
                    return false;
                }
            }
            return true;
        }

        private static void ApplyBaseToZ(double[,] matrix, int[] baseVars, double[] z)
        {
            for (int i = 0; i < baseVars.Length; i++)
            {
                double fact = z[baseVars[i]];
                for (int j = 0; j < z.Length - 1; j++)
                {
                    z[j] -= fact * matrix[i, j];
                }
                z[z.Length - 1] += fact * matrix[i, z.Length - 1];
            }
        }

        public static void Solve(out double outZ, out double[] outVars, out int[] outBase, double[,] matrix, double[] z, params int[] baseVars)
        {
            double[,] myMatrix = (double[,])matrix.Clone();
            double[] myZ = (double[])z.Clone();
            int[] myBase = (int[])baseVars.Clone();
            do
            {
                Simplesso.Gauss(myMatrix, myBase);
                if (!Simplesso.Acceptable(myMatrix))
                {
                    throw new IncoherentProblemException();
                }
                Simplesso.ApplyBaseToZ(myMatrix, myBase, myZ);
                if (!Simplesso.IsBest(myZ))
                {
                    Simplesso.NextBase(myMatrix, myBase, myZ);
                }
            } while (!Simplesso.IsBest(myZ));

            outZ = myZ[myZ.Length - 1];
            outVars = new double[myMatrix.GetLength(1) - 1];
            for (int i = 0; i < outVars.Length; i++)
            {
                outVars[i] = 0;
            }
            for (int i = 0; i < myMatrix.GetLength(0); i++)
            {
                outVars[myBase[i]] = myMatrix[i, myMatrix.GetLength(1) - 1];
            }
            Array.Sort(myBase); // Questo è stato aggiunto all'ultimo per eleganza. Non sembra causare effetti negativi.
            outBase = myBase;
        }

        public static void Solve(out double outZ, out double[] outVars, out int[] outBase, double[,] matrix, double[] z)
        {
            double[,] myMatrix = new double[matrix.GetLength(0), matrix.GetLength(1) + matrix.GetLength(0)];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1) - 1; j++)
                {
                    myMatrix[i, j] = matrix[i, j];
                }
                for (int j = matrix.GetLength(1) - 1; j < myMatrix.GetLength(1) - 1; j++)
                {
                    if (i == j - (matrix.GetLength(1) - 1))
                    {
                        myMatrix[i, j] = 1;
                    }
                    else
                    {
                        myMatrix[i, j] = 0;
                    }
                }
                myMatrix[i, myMatrix.GetLength(1) - 1] = matrix[i, matrix.GetLength(1) - 1];
            }
            int[] myBase = new int[matrix.GetLength(0)];
            for (int i = 0; i < myBase.Length; i++)
            {
                myBase[i] = matrix.GetLength(1) - 1 + i;
            }
            double[] myZ = new double[myMatrix.GetLength(1)];
            for (int i = 0; i < myZ.Length; i++)
            {
                myZ[i] = 0;
            }
            foreach (int item in myBase)
            {
                myZ[item] = -1;
            }

            double subZ;
            double[] subVars;
            int[] subBase;
            Simplesso.Solve(out subZ, out subVars, out subBase, myMatrix, myZ, myBase);
            if (subZ != 0)
            {
                throw new IncoherentProblemException();
            }
            foreach (int item in myBase)
            {
                if (subBase.Contains(item))
                {
                    throw new SimplexException();
                }
            }
            Simplesso.Solve(out outZ, out outVars, out outBase, matrix, z, subBase);
        }
    }
}
