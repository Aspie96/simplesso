#include "Simplesso.hpp"

namespace Simplesso {
	void pivot(double **matrix, int rows, int cols, int i, int j) {
		if(!matrix[i][j]) {
			int row = -1;
			for(int k = rows - 1; k > i; k--) {
				if(matrix[k][j]) {
					row = k;
				}
			}
			for(int k = 0; k < cols; k++) {
				double aux = matrix[row][k];
				matrix[row][k] = matrix[i][k];
				matrix[i][k] = aux;
			}
		}
		double rat = matrix[i][j];
		for(int k = 0; k < cols; k++) {
			matrix[i][k] /= rat;
		}
		for(int k = 0; k < rows; k++) {
			if (k != i) {
				double fact = matrix[k][j];
				for(int l = 0; l < cols; l++) {
					matrix[k][l] -= matrix[i][l] * fact;
				}
			}
		}
	}

	void gauss(double **matrix, int rows, int cols, int *baseVars) {
		for(int i = 0; i < rows; i++) {
			pivot(matrix, rows, cols, i, baseVars[i]);
		}
	}

	bool isBest(double *z, int cols) {
		for(int i = 0; i < cols - 1; i++) {
			if(z[i] > 0) {
				return false;
			}
		}
		return true;
	}

	void nextBase(double **matrix, int rows, int cols, int *baseVars, double *z) {
		int newVar = 0;
		for(int i = 1; i < cols - 1; i++) {
			if(z[i] > z[newVar]) {
				newVar = i;
			}
		}
		double minR = numeric_limits<double>::infinity();
		int index = 0;
		for(int i = 0; i < rows; i++) {
			double r = matrix[i][cols - 1] / matrix[i][newVar];
			if(0 < r && r < minR) {
				index = i;
				minR = r;
			}
		}
		baseVars[index] = newVar;
	}

	void applyBaseToZ(double **matrix, int rows, int cols, int *baseVars, double *z) {
		for(int i = 0; i < rows; i++) {
			double fact = z[baseVars[i]];
			for(int j = 0; j < cols - 1; j++) {
				z[j] -= fact * matrix[i][j];
			}
			z[cols - 1] += fact * matrix[i][cols - 1];
		}
	}

	double solve(double *solution, double **matrix, int rows, int cols, double *z, int *baseVars) {
		do {
			gauss(matrix, rows, cols, baseVars);
			applyBaseToZ(matrix, rows, cols, baseVars, z);
			if (!isBest(z, cols)) {
				nextBase(matrix, rows, cols, baseVars, z);
			}
		} while(!isBest(z, cols));
		memset(solution, 0, sizeof(double) * (cols - 1));
		for(int i = 0; i < rows; i++) {
			solution[baseVars[i]] = matrix[i][cols - 1];
		}
		return z[cols - 1];
	}

	double solve(double *solution, double **matrix, int rows, int cols, double *z) {
		int myCols = rows + cols;
		double *myMatrix[rows];
		for(int i = 0; i < rows; i++) {
			myMatrix[i] = (double*)malloc(sizeof(double) * myCols);
			for(int j = 0; j < cols - 1; j++) {
				myMatrix[i][j] = matrix[i][j];
			}
			for(int j = cols - 1; j < myCols - 1; j++) {
				myMatrix[i][j] = (i == j - (cols - 1));
			}
			myMatrix[i][myCols - 1] = matrix[i][cols - 1];
		}
		double myZ[myCols];
		int myBase[rows];
		memset(myZ, 0, sizeof(double) * rows);
		for (int i = 0; i < rows; i++) {
			myZ[myBase[i] = cols - 1 + i] = -1;
		}
		double subSolution[myCols - 1];
		solve(subSolution, myMatrix, rows, myCols, myZ, myBase);
		for(int i = 0; i < rows; i++) {
			free(myMatrix[i]);
		}
		return solve(solution, matrix, rows, cols, z, myBase);
	}
}
