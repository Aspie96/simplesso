#include <iostream>
#include "Simplesso.hpp"

int main() {
	double z[6] = {3, 2, -5, 0, 0, 0};
	double matrix[2][6] = {{4, -2, 2, 1, 0, 4}, {2, 1, 1, 0, 1, 1}};
	double *matrixPt[2] = {matrix[0], matrix[1]};
	double solution[5];
	int bestZ = Simplesso::solve(solution, matrixPt, 2, 6, z);
	for(int i = 0; i < 5; i++) {
		cout << "x" << i << ": " << solution[i] << endl;
	}
	cout << "Best value of Z: " << bestZ << endl;
}
