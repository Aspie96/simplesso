#ifndef SIMPLESSO_HPP
#define SIMPLESSO_HPP

#include <limits>
#include <stdlib.h>
#include <string.h>

using namespace std;

namespace Simplesso {
	double solve(double*, double**, int, int, double*, int*);
	double solve(double*, double**, int, int, double*);
};

#endif // SIMPLESSO_HPP
